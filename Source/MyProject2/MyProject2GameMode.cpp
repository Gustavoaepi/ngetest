// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "MyProject2GameMode.h"
#include "MyProject2HUD.h"
#include "MyProject2Character.h"
#include "UObject/ConstructorHelpers.h"
#include "BoxObject.h"
#include "Engine/World.h"
#include"Math.h"
//#include <MyProject2\Spawner.h>

AMyProject2GameMode::AMyProject2GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMyProject2HUD::StaticClass();

	SpawnObjects = TArray<ABoxObject*>();
	
}


void AMyProject2GameMode::BeginPlay()
{
	Super::BeginPlay();
	this->InitGameGM();
	
}

void AMyProject2GameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (SpawnObjects.Num() <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("juego terminado"));
	}
	
}



int32 AMyProject2GameMode::CalculatePunch(ABoxObject* aux)
{
	int32 i = 0;
	for (ABoxObject* box : getCollitions(aux))
	{
		i++;
		//box->Destroy();
	}
		
	UE_LOG(LogTemp, Warning, TEXT("valor de id : %d"), i);
	UE_LOG(LogTemp, Warning, TEXT("-------------------"));
	return fibo(i);
	return i;
}



TArray<ABoxObject*>AMyProject2GameMode::getCollitions(ABoxObject* box)
{
	TArray<ABoxObject*> aux = TArray<ABoxObject*>();
	aux.Add(box);
	TArray<ABoxObject*> aux2 = TArray<ABoxObject*>();
	aux2.Add(box);
	for (ABoxObject* box1 : aux2)
	{
		for (ABoxObject* box2 : SpawnObjects)
		{
			if (box1->NumType == box2->NumType)
			{
				if (!aux2.Contains(box2))
				{
					aux2.Add(box2);
				}
			}
		}
	}

	
	
	
	int i = 0;
	int size = aux.Num();
	 int32 DisMin = 150;
	 while(i < size)
	 {
		 for (ABoxObject* box2 : aux2)
		 {
			//float CalcDis = FMath::Sqrt(FMath::Pow(box2->GetTransform().GetLocation().Y - box1->GetTransform().GetLocation().Y, 2) + FMath::Pow(box2->GetTransform().GetLocation().Z - box1->GetTransform().GetLocation().Z, 2));
			float CalcDis = aux[i]->GetDistanceTo(box2);
			//UE_LOG(LogTemp, Warning, TEXT("distancia con Dist %d "), CalcDis);
			if (CalcDis >= -DisMin && CalcDis <= DisMin)
			{
				if (!aux.Contains(box2))
				{
					//UE_LOG(LogTemp, Warning, TEXT("Entro"))
					aux.Add(box2);
				}	
			}
		 }
		 i++;
	}
	 return aux;
}


int32 AMyProject2GameMode::fibo(int32 pos)
{
	TArray<int32> fibo = TArray<int32>();
	fibo.Add(0);
	fibo.Add(1);
	
	//UE_LOG(LogTemp, Warning, TEXT("creo bien "));
	
	try
	{
		for (int32 i = 2; i <= pos; i++)
		{
			fibo.Add(fibo[i - 1] + fibo[i - 2]);
		}
	}
	catch (const std::exception&)
	{
		UE_LOG(LogTemp, Warning, TEXT("Me rompi"));
	}
	
	return fibo[pos];
}


void AMyProject2GameMode::InitGameGM()
{
	if (SpawnObject)
	{
		float y = -1000;
		float otroN = -1000;
		float z = 225;
		int32 max = 7;
		int32 pos = 0;
		//x 600 y 100 z 225
		UWorld* const World = GetWorld();
		FRotator SpawnRotation = FRotator(0);

		for (int32 j = 0; j < 7; j++)
		{
			for (int32 i = 0; i < max; i++)
			{
				FVector SpawnLocation = FVector(600, y, z);
				ABoxObject* aux = World->SpawnActor<ABoxObject>(SpawnObject, SpawnLocation, SpawnRotation);
				aux->NumType = FMath::RandRange(0, Materials.Num() - 1);
				//UE_LOG(LogTemp, Warning, TEXT("Valor: %d"), aux->NumType);
				aux->SetMaterial(Materials[aux->NumType]);
				SpawnObjects.Add(aux);
				y += 100;
			}
			otroN += 50;
			y = otroN;
			z += 100;
			max--;
		}

		UE_LOG(LogTemp, Warning, TEXT("tamanio array: %d "), SpawnObjects.Num());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No existe elemento a spawnear"));
	}
}
