// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BoxObject.generated.h"

class UStaticMeshComponent;
class UMaterial;


UCLASS()
class MYPROJECT2_API ABoxObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABoxObject();

protected:


	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* MeshComp;

	virtual void BeginPlay() override;

	

public:	

	UPROPERTY(EditAnywhere, Category = "Components")
	int32 NumType;

	UPROPERTY(EditAnywhere, Category = "Components")
	class AMyProject2GameMode* GM;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void SetMaterial(UMaterial* Material);

	UFUNCTION()
	void OnCollisionBullet();



};
