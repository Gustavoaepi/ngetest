// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyProject2GameMode.generated.h"


class ABoxObject;



UCLASS(minimalapi)
class AMyProject2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMyProject2GameMode();

	UFUNCTION()
	int32 CalculatePunch(ABoxObject* aux);
	

private :



	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = "Config")
	TSubclassOf<ABoxObject> SpawnObject;


	UPROPERTY(EditAnywhere, Category = "Config")
	TArray<UMaterial*> Materials;

	UPROPERTY(EditAnywhere, Category = "Config")
	TArray<ABoxObject*> SpawnObjects;

	UFUNCTION()
	TArray<ABoxObject*> getCollitions(ABoxObject* box);
	UFUNCTION()
	int32 fibo(int32 pos);
	
	UFUNCTION()
	void InitGameGM();

};



