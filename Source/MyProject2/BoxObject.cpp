// Fill out your copyright notice in the Description page of Project Settings.


#include "BoxObject.h"
#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Materials/MaterialInstance.h"
#include "MyProject2GameMode.h"
#include <MyProject2\MyProject2Projectile.h>

// Sets default values
ABoxObject::ABoxObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	RootComponent = MeshComp;
	//MeshComp->OnComponentHit.AddDynamic(this,&ABoxObject::OnCompHit);
	

	SetReplicates(true);
	SetReplicateMovement(true);

	

}

// Called when the game starts or when spawned
void ABoxObject::BeginPlay()
{
	Super::BeginPlay();
	GM = Cast<AMyProject2GameMode>(GetWorld()->GetAuthGameMode());
	//UE_LOG(LogTemp, Warning, TEXT("HOLA"));
	//MeshComp->SetMaterial(0, Material);
	
}

// Called every frame
void ABoxObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABoxObject::SetMaterial(UMaterial* Material)
{
	MeshComp->SetMaterial(0, Material);
	//UE_LOG(LogTemp, Warning, TEXT("Cambie"));
}

void ABoxObject::OnCollisionBullet()
{

	//UE_LOG(LogTemp, Warning, TEXT("%s"), *FString("entro a la wea"));
	UE_LOG(LogTemp, Warning, TEXT("EL TIPO ES : %d "), NumType);
	try
	{
		if (GM)
		{
			GM->CalculatePunch(this);
		}
	}
	catch (const std::nullptr_t&)
	{
		UE_LOG(LogTemp, Warning, TEXT("no existe el mundo"));
	}
}


