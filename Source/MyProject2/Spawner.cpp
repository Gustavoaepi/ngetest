// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "BoxObject.h"
#include "Engine/World.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SpawnObjects = TArray<ABoxObject*>();
	 

}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//UE_LOG(LogTemp, Warning, TEXT("tamanio array: %d "), SpawnObjects.Num());

}


int32 ASpawner::CalculatePunch(ABoxObject* aux)
{

	UE_LOG(LogTemp, Warning, TEXT("tamanio array: %d "), SpawnObjects.Num());

	for (ABoxObject* box : SpawnObjects)
	{
		UE_LOG(LogTemp, Warning, TEXT("Valor aux: %d , i spawn %d"), aux->NumType, box->NumType);

		if (box->NumType == aux->NumType)
		{
			box->Destroy();
		}
	}


	UE_LOG(LogTemp, Warning, TEXT("El singleton funciona"));
	return -1;
}



ASpawner* ASpawner::instance;

ASpawner* ASpawner::Instance()
{
	if (!instance)
	{
		instance = NewObject<ASpawner>();
	}
	return instance;
}



