// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "MyProject2Projectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include <MyProject2\BoxObject.h>
#include "Spawner.h"
#include <MyProject2\MyProject2GameMode.h>
#include "Engine/World.h"

AMyProject2Projectile::AMyProject2Projectile() 
{

	BoxObject = nullptr;
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &AMyProject2Projectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

	SetReplicates(true);
	SetReplicateMovement(true);
}

void AMyProject2Projectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
	{
		
			//OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());	
		try
		{
			if (BoxObject == nullptr)
			{
				BoxObject = Cast<ABoxObject>(OtherActor);
				if (BoxObject)
				{
					UE_LOG(LogTemp, Warning, TEXT("%s"), *FString(BoxObject->GetName()));
					BoxObject->OnCollisionBullet();
				}
				
			}
		}
		catch (const std::exception&)
		{
			UE_LOG(LogTemp, Warning, TEXT("%s"), *FString("Me Rompi"));
		}
		
	}	
	BoxObject = nullptr;
	this->Destroy();
}