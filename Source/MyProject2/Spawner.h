// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

class ABoxObject;
class UMaterial;

UCLASS()
class MYPROJECT2_API ASpawner : public AActor
{
	GENERATED_BODY()

private:
	static ASpawner* instance;



public:	
	// Sets default values for this actor's properties
	ASpawner();

protected:

	//UPROPERTY(EditAnywhere, Category = "Config")
	//ABoxObject* SpawnObject;

	UPROPERTY(EditAnywhere, Category = "Config")
	TSubclassOf<ABoxObject> SpawnObject;


	UPROPERTY(EditAnywhere, Category = "Config")
	TArray<UMaterial*> Materials;

	UPROPERTY(EditAnywhere, Category = "Config")
	TArray<ABoxObject*> SpawnObjects;



	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	static ASpawner* Instance();

	UFUNCTION()
	int32 CalculatePunch(ABoxObject* aux);

};
